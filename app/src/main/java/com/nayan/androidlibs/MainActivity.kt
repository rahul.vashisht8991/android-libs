package com.nayan.androidlibs

import android.content.Context
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.nayan.androidlibs.data.models.Item
import com.nayan.androidlibs.data.models.Repo
import com.nayan.androidlibs.data.remote.APIService
import com.nayan.androidlibs.data.remote.ApiUtils
import kotlinx.android.synthetic.main.activity_main.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class MainActivity : AppCompatActivity() {

    var context: Context? = null
    private var mAPIService: APIService? = null
    var repositoryList: MutableList<Item> = arrayListOf()
    lateinit  var adapter: RepositoryItemViewHolder

    private var layoutManager: LinearLayoutManager? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_main)

        context = this

        supportActionBar?.title = "Android Git Repo"

        mAPIService = ApiUtils.apiService

        setupRecyclerView()

        fetchRepository()
    }

    // fetching repo
    private fun fetchRepository() {

        mAPIService?.getRepositories()?.enqueue(object : Callback<Repo> {
            override fun onFailure(call: Call<Repo>?, t: Throwable?) {

            }

            override fun onResponse(call: Call<Repo>?, response: Response<Repo>?) {
                if (response != null) {

                    if (response.isSuccessful) {

                        var repo = response.body()
                        repositoryList.addAll(repo?.items!!)
                        adapter.repositories = repositoryList
                        adapter.notifyDataSetChanged()

                    }
                }
            }

        })
    }

    private fun setupRecyclerView() {
        val manager = GridLayoutManager(context, 2, GridLayoutManager.VERTICAL, false)
        layoutManager = LinearLayoutManager(context)
        recyclerView.layoutManager = layoutManager
        adapter  = RepositoryItemViewHolder(repositoryList)
        recyclerView.adapter = adapter
    }


}