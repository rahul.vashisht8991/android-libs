package com.nayan.androidlibs.data.models

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class Owner {


    @SerializedName("id")
    @Expose
    var id: Int = 0
    @SerializedName("avatar_url")
    @Expose
    var avatarUrl: String? = null
    @SerializedName("url")
    @Expose
    var url: String? = null
    @SerializedName("html_url")
    @Expose
    var htmlUrl: String? = null
    @SerializedName("type")
    @Expose
    var type: String? = null
    @SerializedName("site_admin")
    @Expose
    var isSiteAdmin: Boolean = false

}
