package com.nayan.androidlibs
.data.remote

import com.nayan.androidlibs.data.models.Repo
import retrofit2.Call
import retrofit2.http.GET

interface APIService {

    @GET("search/repositories?q=android%20language:java&sort=stars&order=desc&per_page=40&page=1")
    fun getRepositories(): Call<Repo>
}
